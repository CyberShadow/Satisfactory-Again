// Copyright Coffee Stain Studios. All Rights Reserved.


#include "AgainGameWorldModule.h"

#include "Engine/Engine.h"

#include "FGCharacterPlayer.h"
#include "FGPlayerController.h"
#include "FGChatManager.h"
#include "Equipment/FGBuildGun.h"
#include "Equipment/FGBuildGunBuild.h"
#include "Hologram/FGHologram.h"

#include "Patching/NativeHookManager.h"

#include "AgainModule.h"

UAgainGameWorldModule* UAgainGameWorldModule::Instance = nullptr;

UAgainGameWorldModule::UAgainGameWorldModule()
{
#if !WITH_EDITOR
	this->bRootModule = true;

	if (!this->GetWorld()) {
		return;
	}

	UAgainGameWorldModule::Instance = this;
#endif
}

UAgainGameWorldModule::~UAgainGameWorldModule()
{
#if !WITH_EDITOR
	UAgainGameWorldModule::Instance = nullptr;
#endif
}

void UAgainGameWorldModule::OnRecordPressed()
{
	AFGBuildGun* buildGun = this->PlayerCharacter->GetBuildGun();
	FHitResult hitResult;
	buildGun->TraceForBuilding(this->PlayerCharacter.Get(), hitResult);
	AActor* anchorActor = hitResult.Actor.Get();
	if (!anchorActor) {
		ShowMessage(FString::Printf(TEXT("Error - not looking at anything. Please look at an object to use as anchor.")));
		return;
	}

	if (!CurrentRecording.Active) {
		CurrentRecording = {};
		CurrentRecording.AnchorLocations[0] = anchorActor->GetActorLocation();
		CurrentRecording.AnchorType = anchorActor->GetClass();
		CurrentRecording.Active = true;
		ShowMessage(FString::Printf(TEXT("Recording started")));
	} else {
		if (anchorActor->GetClass() != CurrentRecording.AnchorType) {
			ShowMessage(FString::Printf(TEXT("Anchor object type mismatch: you started with %s, but are now looking at %s"),
				*CurrentRecording.AnchorType->GetName(),
				*anchorActor->GetClass()->GetName()
			));
			return;
		}

		CurrentRecording.AnchorLocations[1] = anchorActor->GetActorLocation();
		CurrentRecording.AnchorType = anchorActor->GetClass();
		CurrentRecording.Active = false;
		ShowMessage(FString::Printf(TEXT("Recording finished")));
	}
}

static AFGHologram* DeserializeHologram(FConstructHologramMessage& message, UObject* outer) {
	TSubclassOf<UFGItemDescriptor> descriptorClass = UFGRecipe::GetDescriptorForRecipe(message.Recipe);
	UClass* hologramClass = UFGBuildDescriptor::GetHologramClass(descriptorClass.Get());
	AFGHologram* hologram = NewObject<AFGHologram>(outer, hologramClass);

	FBitReader reader;
	TArray<uint8_t> data = message.SerializedHologramData; // copy to allow passing by move-reference
	reader.SetData(std::move(data), message.NumBits);
	hologram->ServerPreConstructMessageDeserialization();
	hologram->SerializeConstructMessage(reader, message.ConstructionID);
	hologram->ServerPostConstructMessageDeserialization();

	return hologram;
}

static void SerializeHologram(AFGHologram* hologram, FConstructHologramMessage& message) {
	FBitWriter writer(1024, true);
	hologram->ClientPreConstructMessageSerialization();
	hologram->SerializeConstructMessage(writer, message.ConstructionID);
	hologram->ClientPostConstructMessageSerialization();

	message.SerializedHologramData = *writer.GetBuffer();
	message.NumBits = writer.GetNumBits();
}

static void PatchHologramMessage(FConstructHologramMessage& message, UObject* outer, const FVector& from, const FVector& to) {
	FVector delta = to - from;

	AFGHologram* hologram = DeserializeHologram(message, outer);

	hologram->SetActorLocation(hologram->GetActorLocation() + delta);

	SerializeHologram(hologram, message);
}

void UAgainGameWorldModule::OnPlayPressed()
{
	if (CurrentRecording.Active) {
		ShowMessage(FString::Printf(TEXT("Still recording! Finish recording first.")));
		return;
	}

	AFGBuildGun* buildGun = this->PlayerCharacter->GetBuildGun();
	FHitResult hitResult;
	buildGun->TraceForBuilding(this->PlayerCharacter.Get(), hitResult);
	AActor* anchorActor = hitResult.Actor.Get();
	if (!anchorActor) {
		ShowMessage(FString::Printf(TEXT("Error - not looking at anything. Please look at an object to use as anchor.")));
		return;
	}

	if (anchorActor->GetClass() != CurrentRecording.AnchorType) {
		ShowMessage(FString::Printf(TEXT("Anchor object type mismatch: the recording uses %s, but are now looking at %s"),
			*CurrentRecording.AnchorType->GetName(),
			*anchorActor->GetClass()->GetName()
		));
		return;
	}

	UFGBuildGunStateBuild* buildState = Cast<UFGBuildGunStateBuild>(buildGun->GetState(EBuildGunState::BGS_BUILD));
	for (FConstructHologramMessage message : CurrentRecording.Buildings) {
		AFGBuildableSubsystem* buildableSubsystem = AFGBuildableSubsystem::Get(this->GetWorld());
		FNetConstructionID constructID = buildableSubsystem->GetNewNetConstructionID();
		message.ConstructionID = constructID;
		PatchHologramMessage(message, this->GetWorld(), CurrentRecording.AnchorLocations[0], anchorActor->GetActorLocation());
		buildState->Server_ConstructHologram(constructID, message);
	}

	ShowMessage("You pressed Play!");
}

void UAgainGameWorldModule::OnServer_ConstructHologram(FNetConstructionID clientNetConstructID, FConstructHologramMessage message)
{
	UE_LOG(LogAgain, Display, TEXT("UFGBuildGunStateBuild::Server_ConstructHologram(FNetConstructionID(%d, %d, %d), FConstructHologramMessage(FNetConstructionID(%d, %d, %d), '%s', [%d bits]))"),
		clientNetConstructID.NetPlayerID, clientNetConstructID.Server_ID, clientNetConstructID.Client_ID,
		message.ConstructionID.NetPlayerID, message.ConstructionID.Server_ID, message.ConstructionID.Client_ID,
		*UFGRecipe::GetRecipeName(message.Recipe).ToString(),
		message.NumBits
	);

	if (CurrentRecording.Active) {
		CurrentRecording.Buildings.Push(message);
		ShowMessage(FString::Printf(TEXT("Added %s to recording."), *UFGRecipe::GetRecipeName(message.Recipe).ToString()));
	}
}

void UAgainGameWorldModule::ShowMessage(FString Message, FLinearColor Color)
{
	AFGChatManager* ChatManager = AFGChatManager::Get(this->GetWorld());
	FChatMessageStruct MessageStruct;
	MessageStruct.MessageString = FString::Printf(TEXT("[Again] %s"), *Message);
	MessageStruct.MessageType = EFGChatMessageType::CMT_SystemMessage;
	MessageStruct.ServerTimeStamp = this->GetWorld()->TimeSeconds;
	MessageStruct.CachedColor = Color;
	ChatManager->AddChatMessageToReceived(MessageStruct);
}
