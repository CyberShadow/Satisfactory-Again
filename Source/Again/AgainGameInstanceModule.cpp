// Copyright Coffee Stain Studios. All Rights Reserved.


#include "AgainGameInstanceModule.h"

#include "AgainModule.h"

#define LOCTEXT_NAMESPACE "Again"

UAgainGameInstanceModule::UAgainGameInstanceModule()
{
#if !WITH_EDITOR
	this->bRootModule = true;

#define ADD_KEY(name, id, key, shift, ctrl, alt, cmd)					\
	this->ModKeyBindings.Push({											\
		"Again." ## id,													\
		FInputActionKeyMapping("Again." ## id, key, shift, ctrl, alt, cmd), \
		LOCTEXT("Action" ## id, "Again - " ## name)						\
	})

	ADD_KEY("Record", "Record", EKeys::MiddleMouseButton, false, true , true , false);
	ADD_KEY("Play"  , "Play"  , EKeys::MiddleMouseButton, false, true , false, false);
#undef ADD_KEY

#endif
}

#undef LOCTEXT_NAMESPACE
