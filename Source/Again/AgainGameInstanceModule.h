// Copyright Coffee Stain Studios. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Module/GameInstanceModule.h"
#include "AgainGameInstanceModule.generated.h"

/**
 * 
 */
UCLASS()
class AGAIN_API UAgainGameInstanceModule : public UGameInstanceModule
{
	GENERATED_BODY()

public:
	UAgainGameInstanceModule();
};
