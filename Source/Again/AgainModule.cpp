#include "AgainModule.h"

#include "Equipment/FGBuildGun.h"
#include "Equipment/FGBuildGunBuild.h"
#include "FGCharacterPlayer.h"

#include "Patching/NativeHookManager.h"

#include "AgainGameWorldModule.h"

DEFINE_LOG_CATEGORY(LogAgain)

void FAgainModule::StartupModule() {
#if !WITH_EDITOR

	#define CHECK_INSTANCE \
		if (!UAgainGameWorldModule::Instance) { \
			UE_LOG(LogAgain, Error, TEXT("No UAgainGameWorldModule::Instance?")); \
			return;														\
		}

	SUBSCRIBE_METHOD_VIRTUAL(AFGCharacterPlayer::SetupPlayerInputComponent, GetMutableDefault<AFGCharacterPlayer>(), [](auto& scope, AFGCharacterPlayer* self, UInputComponent* InputComponent) {
		CHECK_INSTANCE
		UAgainGameWorldModule::Instance->PlayerCharacter = self;
	});

	SUBSCRIBE_METHOD_VIRTUAL(APlayerController::PushInputComponent, GetMutableDefault<AFGPlayerController>(), [](auto& scope, APlayerController* self, UInputComponent* InputComponent) {
		CHECK_INSTANCE
		InputComponent->BindAction("Again.Record", IE_Pressed, UAgainGameWorldModule::Instance, &UAgainGameWorldModule::OnRecordPressed);
		InputComponent->BindAction("Again.Play"  , IE_Pressed, UAgainGameWorldModule::Instance, &UAgainGameWorldModule::OnPlayPressed  );
	});

	SUBSCRIBE_METHOD(UFGBuildGunStateBuild::Server_ConstructHologram, [](auto& scope, UFGBuildGunStateBuild* self, FNetConstructionID clientNetConstructID, FConstructHologramMessage data) {
		CHECK_INSTANCE
		UAgainGameWorldModule::Instance->OnServer_ConstructHologram(clientNetConstructID, data);
	});
#endif
}

IMPLEMENT_GAME_MODULE(FAgainModule, Again);
