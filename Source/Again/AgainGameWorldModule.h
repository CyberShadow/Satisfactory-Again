// Copyright Coffee Stain Studios. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Module/GameWorldModule.h"
#include "FGCharacterPlayer.h"
#include "FGConstructionMessageInterface.h"
#include "AgainGameWorldModule.generated.h"

USTRUCT()
struct AGAIN_API FAgainRecording
{
    GENERATED_BODY()

	FAgainRecording() : Active(false) {}

	bool Active; // Is recording?

	FVector AnchorLocations[2];

	UClass* AnchorType;

	TArray<FConstructHologramMessage> Buildings;
};

/**
 * 
 */
UCLASS()
class AGAIN_API UAgainGameWorldModule : public UGameWorldModule
{
	GENERATED_BODY()

	FAgainRecording CurrentRecording;

public:
	UAgainGameWorldModule();
	virtual ~UAgainGameWorldModule();

	static UAgainGameWorldModule* Instance;

	TWeakObjectPtr<AFGCharacterPlayer> PlayerCharacter;
	void OnRecordPressed();
	void OnPlayPressed();
	void OnServer_ConstructHologram(FNetConstructionID clientNetConstructID, FConstructHologramMessage data);

	void ShowMessage(FString Message, FLinearColor Color = FLinearColor::Red);
};
